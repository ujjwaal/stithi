##1) The system needs the electronic board which is a hardware developed by us of which the ckt designs have been submitted. We can courier the board if required.

##2) The board needs the battery to run or directly connect with usb cable with arduino platform which can be installed in any os.

##Codes can be compiled and tested without any hardware requirements :-

##3) If you want to test the codes only :-

a) Install Arduino software in PC (available for any OS) 
b) Open the Arduino folder / Stithi_Dispensor. There will be bunch of files available. 
c) Run any of the file in Arduino Platform . It will run without any error. 
d) There will some .ZIP libraries in sketch folder inside Arduino folder. Add it in sketch tab (Sketch -> Include lIbrarry -> add .ZIP libraries) 
in Arduino platform if required or error comes during compiling codes. 
e) Select tools -> Boards -> Arduino/Genuino Uno (If error arises during compilation)

##4) Running Dashboard:- Dashboard is created to give look and feel of complete system 
a) Create a DB Named stithi 
b) Import SQL file. (stithi.sql available in main folder) 
c)Copy files and folders from Dashboard to htdocs fodler 
d) change configuration in connect.php according to yours 
e)Open in browser localhost/"folder name accroding to yours"

##5) Running Web app. Web app is created to check real time data coming from device. It can also be seen on http://34.210.26.48/ 
a) Create a DB Named stithi 
b) Import SQL file. 
c)Copy files and folders from Web App to htdocs fodler 
d) change configuration in connect.php according to yours 
e)Open in browser localhost/"folder name accroding to yours"