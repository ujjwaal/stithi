<?php

if(!isset($_SERVER['HTTP_X_PJAX'])){

    $content = basename($_SERVER['SCRIPT_NAME']);

    $_SERVER['HTTP_X_PJAX'] = true;
    include 'stilearn.base.template.php';
    die();
}

?>
                    <!-- CALENDAR
                    ====================== -->
<?php
try
  {
    require 'connect.php';
    $user=$_COOKIE['user'];
    $allresult = $conn->prepare("SELECT `title`,`start`,`end` FROM `calendar` WHERE `school` = :username OR `school` = '7828476732'");  //This query will result the attendance record of the logged in school of this year date wise in desc order
    $allresult->bindParam(':username', $user, PDO::PARAM_STR);
    $allresult->execute();
    $totalcount=$allresult->fetchAll();
  }
  catch(PDOException $q)
  {
     echo "Error:" . $q->getMessage();
  }
  $conn = null;
?>


                    <div class="row">
                    	<div class="col-md-9">
                      <div class="panel panel-default">
                    			<div class="panel-heading bg-white">
                    				<div class="panel-actions">
                                        <button id="calendar-viewtoday" class="btn-panel">Today</button>
                                        <button id="calendar-viewmonth" class="btn-panel active">Month</button>
                                        <button id="calendar-viewweek" class="btn-panel">Week</button>
                                        <button id="calendar-viewday" class="btn-panel">Day</button>
                                    </div><!--panel-actions-->
                    				<h4 class="panel-title" id="calendar-viewtitle"><i class="fa fa-spinner fa-spin"></i></h4>
                    			</div><!--panel-heading-->

								<div class="panel-helper-block">
                    				<div class="btn-toolbar" role="toolbar">
                    					<div class="btn-group btn-group-sm pull-right">
									  		<button class="btn btn-default" id="calendar-viewnext">Next</button>
									  		<button class="btn btn-default" id="calendar-viewnextYear">Next Year</button>
                    					</div>
                    					<div class="btn-group btn-group-sm pull-left">
									  		<button class="btn btn-default" id="calendar-viewprevYear">Prev Year</button>
									  		<button class="btn btn-default" id="calendar-viewprev">Prev</button>
								  		</div>
									</div>
								</div><!--/panel-helper-block-->

                    			<div class="panel-body">
                    				<div id="calendar" class="table-responsive"></div>
                    			</div><!--panel-body-->
                    		</div><!--panel-->
                     </div><!--cols-->

                    	<div class="col-md-3">
                    		<div id="external-events" class="panel panel-default">
                    			<div class="panel-heading bg-white">
                    				<div class="panel-actions">
                                        <button title="Add event" class="btn-panel disable-tooltip">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div><!--panel-actions-->
                    				<h4 class="panel-title">Important Events</h4>
                    			</div><!--panel-heading-->

                    			<div class="list-group">
                    				<div class='list-group-item external-event'>Staff Meeting (8:30 am)</div>
									<div class='list-group-item external-event'>Event 2</div>
									<div class='list-group-item external-event'>Event 3</div>
									<div class='list-group-item external-event'>Event 4</div>
                    			</div><!--list-group-->

                    			<div class="panel-footer">
                                                 <div class="nice-checkbox" style="display:none">
                                                    <input type='checkbox' id='drop-remove' checked />
                                                     <label for='drop-remove'>Drag And Save</label>
                                                 </div>
                                                 <button id="savecalevntsload" type="button" class="btn btn-success btn-block btn-lg" style="display:none">
                                                    <i class="fa fa-spinner fa-spin"></i> Saving Changes
                                                 </button>
                                                 <button id="savecalevnts" type="button" class="btn btn-icon btn-success btn-block btn-lg" style="display:none">
                                                    <i class="fa fa-check-circle"></i> Changes Saved
                                                 </button>
                    			</div><!--panel-footer-->
                    		</div><!--panel-->
                    	</div><!--cols-->
                    </div><!--row-->
                    

<script>
                    
                    $(function(){
	'use strict';

        var sendtitle,sendstdate,sendenddate;

        var updateevents = <?php echo json_encode($totalcount); ?> ;

    // FULL CALENDAR DEMO
        var demo_calendar = function(){
        /* initialize the calendar
        -----------------------------------------------------------------*/
        var calendar = $('#calendar').fullCalendar({
            header: false,
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                    sendtitle =  title;
                    sendstdate = start;
                    sendenddate = end ;
                    insertdbcalendar(sendtitle,sendstdate,sendenddate);
                }
                calendar.fullCalendar('unselect');
            },
            editable: true,
            events: updateevents,
            eventResize: function(event) {

                    sendtitle =  event.title;
                    sendstdate = event.start;
                    if(event.end == null)
                     {
                       sendenddate = event.start ;
                     }
                    else
                     {
                       sendenddate = event.end ;
                     }
                    
                    updatedbcalendar(sendtitle,sendstdate,sendenddate);
            },
            eventDrop: function(event) {

                    sendtitle =  event.title;
                    sendstdate = event.start;
                    if(event.end == null)
                     {
                       sendenddate = event.start ;
                     }
                    else
                     {
                       sendenddate = event.end ;
                     }
                    
                      updatedbcalendar(sendtitle,sendstdate,sendenddate);
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                    sendtitle =  $(this).text();

                }
                    sendstdate = date;
                    sendenddate = date ;
                    insertdbcalendar(sendtitle,sendstdate,sendenddate);
            }
        });

        // Calendar control
        $('#calendar-viewtitle').text($('#calendar').fullCalendar('getView').title);
        
        $(document).on('click', '#calendar-viewmonth', function(){
            $('#calendar').fullCalendar( 'changeView', 'month' );

            $(this).parent().find('.btn-panel').removeClass('active');
            $(this).addClass('active');
        });
        $(document).on('click', '#calendar-viewweek', function(){
            $('#calendar').fullCalendar( 'changeView', 'agendaWeek' )
                .find('.fc-agenda > div').children('div:last-child').niceScroll({
                    cursorcolor: 'rgba(0, 0, 0, .1)',
                    cursorwidth: '12px',
                    cursorborder: '3px solid transparent',
                    cursorborderradius: '6px'
                });

            $(this).parent().find('.btn-panel').removeClass('active');
            $(this).addClass('active');
        });
        $(document).on('click', '#calendar-viewday', function(){
            $('#calendar').fullCalendar( 'changeView', 'agendaDay' )
                .find('.fc-agenda > div').children('div:last-child').niceScroll({
                    cursorcolor: 'rgba(0, 0, 0, .1)',
                    cursorwidth: '12px',
                    cursorborder: '3px solid transparent',
                    cursorborderradius: '6px'
                });

            $(this).parent().find('.btn-panel').removeClass('active');
            $(this).addClass('active');
        });

        $(document).on('click', '#calendar-viewtoday', function(){
            $('#calendar').fullCalendar( 'today' );
        });
        $(document).on('click', '#calendar-viewnext', function(e){
            e.preventDefault();
            $('#calendar').fullCalendar('next');
        });
        $(document).on('click', '#calendar-viewnextYear', function(e){
            e.preventDefault();
            $('#calendar').fullCalendar('nextYear');
        });
        $(document).on('click', '#calendar-viewprev', function(e){
            e.preventDefault();
            $('#calendar').fullCalendar('prev');
        });
        $(document).on('click', '#calendar-viewprevYear', function(e){
            e.preventDefault();
            $('#calendar').fullCalendar('prevYear');
        });

        $(document).on('click', '#calendar-viewtoday, #calendar-viewmonth, #calendar-viewweek, #calendar-viewday, #calendar-viewnext, #calendar-viewprev, #calendar-viewnextYear, #calendar-viewprevYear', function(){
            var view = $('#calendar').fullCalendar('getView'),
                title = view.title;

            title = title.replace('&#8212;', 'to');
            $('#calendar-viewtitle').text(title);
        });

        /* initialize the external events
        -----------------------------------------------------------------*/
        $('#external-events div.external-event').each(function() {
        
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
            
        });
    }
    // initialize demo calendar
    if($(document).find('#calendar').length > 0){
        demo_calendar();
    }
    // END FULL CALENDAR DEMO
    
    function updatedbcalendar(sendtitle,sendstdate,sendenddate)
     {
       $("#savecalevntsload").show(500);
       var startmonth = +sendstdate.getMonth() + 1;
       var endmonth = +sendenddate.getMonth() + 1;
       sendstdate  = sendstdate.getFullYear()+'-'+startmonth+'-'+sendstdate.getDate();
       sendenddate = sendenddate.getFullYear()+'-'+endmonth+'-'+sendenddate.getDate();
       var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          $("#savecalevntsload").hide();
          var response = xmlhttp.responseText;
           if(response == 'updated')
            {
              $("#savecalevnts").show(500);
            }
        }
     };
     xmlhttp.open('POST','attresponse.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('title='+sendtitle+'&start='+sendstdate+'&end='+sendenddate);
     };
     
     function insertdbcalendar(sendtitle,sendstdate,sendenddate)
      {
        $("#savecalevntsload").show(500);
       var startmonth = +sendstdate.getMonth() + 1;
       var endmonth = +sendenddate.getMonth() + 1;
       sendstdate  = sendstdate.getFullYear()+'-'+startmonth+'-'+sendstdate.getDate();
       sendenddate = sendenddate.getFullYear()+'-'+endmonth+'-'+sendenddate.getDate();
       var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          $("#savecalevntsload").hide();
          var response = xmlhttp.responseText;
           if(response == 'inserted')
            {
              $("#savecalevnts").show(500);
            }
        }
     };
     xmlhttp.open('POST','attresponse.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('institle='+sendtitle+'&insstart='+sendstdate+'&insend='+sendenddate);
      }

});
</script>
