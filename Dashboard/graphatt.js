google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBackgroundColor);


 var attendance = [
        ['Day 1', 32],  ['Day 2', 32],  ['Day 3', 27],  ['Day 4', 30],  ['Day 5', 31],
        ['Day 6', 29],  ['Day 7', 33],  ['Day 8', 31],  ['Day 9', 31],  ['Day 10', 33], ['Day 11', 32],
        ['Day 12', 31], ['Day 13', 35], ['Day 14', 30], ['Day 15', 28], ['Day 16', 33], ['Day 17', 32],
        ['Day 18', 35]
      ];

function drawBackgroundColor() {
      var data = new google.visualization.DataTable();

      data.addColumn('string', 'Days');
      data.addColumn('number', 'Attendance');

      var options = {
        width: 1200,
        height: 400,
        animation:{
         duration: 1000,
         easing: 'in',
        },
        hAxis: {
          title: 'Days'
        },
        vAxis: {
          title: 'Attendance'
        },
        backgroundColor: '#f1f8e9'
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      var current = 0;

    function drawChart() {
      // Disabling the button while the chart is drawing.

      data.addRows(attendance);

      chart.draw(data, options);
    }

    $("#nmc19").click(function()
    {
      $("#schoolnm").html("Patient Adherence Graph (House 1)");
      attendance = [
        ['Day 1', 34],  ['Day 2', 33],  ['Day 3', 35],  ['Day 4', 34],  ['Day 5', 38],
        ['Day 6', 37],  ['Day 7', 40],  ['Day 8', 39]
      ];
      data.removeRows(0, data.getNumberOfRows())
      drawChart();

    });
    
    $("#nmc125").click(function()
    {
      $("#schoolnm").html("Patient Adherence Graph (House 2)");
      attendance = [
        ['Day 1', 27],  ['Day 2', 31],  ['Day 3', 38],  ['Day 4', 26],  ['Day 5', 29],
        ['Day 6', 30],  ['Day 7', 30]
      ];
      data.removeRows(0, data.getNumberOfRows())
      drawChart();

    });
    
    $("#nmc83").click(function()
    {
      $("#schoolnm").html("Patient Adherence Graph (House 3)");
      attendance = [
        ['Day 1', 32],  ['Day 2', 32],  ['Day 3', 27],  ['Day 4', 30],  ['Day 5', 31],
        ['Day 6', 29],  ['Day 7', 33],  ['Day 8', 31],  ['Day 9', 31],  ['Day 10', 33], ['Day 11', 32],
        ['Day 12', 31], ['Day 13', 35], ['Day 14', 30], ['Day 15', 28], ['Day 16', 33], ['Day 17', 32],
        ['Day 18', 35]
      ];
      data.removeRows(0, data.getNumberOfRows())
      drawChart();

    });

    drawChart();


      //var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      //chart.draw(data, options);
    }
