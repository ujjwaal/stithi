<?php
require 'logininfo.php';
  if(loggedin()===false):
 {die(header('Location:index.php'));}
  endif;
  function test_input($data)
  {
   $data = stripslashes($data);
   $data = htmlentities($data);
   $data = htmlspecialchars($data);
   $data = ucwords(strtolower($data));
   return $data;
  };
if($_SERVER["REQUEST_METHOD"] == "POST"):
 {
   date_default_timezone_set("Asia/Kolkata");
    $currdate =  date('Y/m/d');

   if( isset($_POST['schoolid'])&& !empty($_POST['schoolid']) ):
     {
  try
  {
    require 'connect.php';
    $user=$_SESSION['username'];
    $currslt = $conn->prepare("SELECT COUNT(subid) AS tdattcnt FROM student JOIN school ON student.galid = school.galid WHERE student.subid IN ( SELECT attendance.subid FROM attendance WHERE attendance.recdate = '2017-04-06' ) AND school.schoolid =:username GROUP BY student.galid");
    $currslt->bindParam(':username', $user, PDO::PARAM_STR);
    $currslt->execute();
    $dayresult=$currslt->fetchAll();
  }
  catch(PDOException $q)
  {
    echo "Error:" . $q->getMessage();
  }
   $conn = null;
  
  $todaycount = 0;

     foreach($dayresult as $values)
      {
        $todaycount = $todaycount + $values['tdattcnt'];
      }
     echo $todaycount;
    }
    elseif(isset($_POST['title'])&& !empty($_POST['title']) && isset($_POST['start'])&& !empty($_POST['start']) && isset($_POST['end'])&& !empty($_POST['end'])):
     {
       $title =  test_input($_POST['title']);
       $start =  test_input($_POST['start']);
       $end =  test_input($_POST['end']);
       try
       {
         require 'connect.php';
         $user=$_SESSION['username'];
         $currslt = $conn->prepare("UPDATE `calendar` SET `start`=:start,`end`=:end WHERE `school` = :username AND `title` = :title");
         $currslt->bindParam(':username', $user, PDO::PARAM_STR);
         $currslt->bindParam(':title', $title, PDO::PARAM_STR);
         $currslt->bindParam(':start', $start, PDO::PARAM_STR);
         $currslt->bindParam(':end', $end, PDO::PARAM_STR);
         $currslt->execute();
         echo 'updated';
       }
       catch(PDOException $q)
       {
         echo "Error:" . $q->getMessage();
       }
       $conn = null;
     }
     elseif(isset($_POST['institle'])&& !empty($_POST['institle']) && isset($_POST['insstart'])&& !empty($_POST['insstart']) && isset($_POST['insend'])&& !empty($_POST['insend'])):
     {
       $title =  test_input($_POST['institle']);
       $start =  test_input($_POST['insstart']);
       $end =  test_input($_POST['insend']);
       try
       {
         require 'connect.php';
         $user=$_SESSION['username'];
         $currslt = $conn->prepare("INSERT INTO `calendar`(`school`, `title`, `start`, `end`) VALUES (:username,:title,:start,:end)");
         $currslt->bindParam(':username', $user, PDO::PARAM_STR);
         $currslt->bindParam(':title', $title, PDO::PARAM_STR);
         $currslt->bindParam(':start', $start, PDO::PARAM_STR);
         $currslt->bindParam(':end', $end, PDO::PARAM_STR);
         $currslt->execute();
         echo 'inserted';
       }
       catch(PDOException $q)
       {
         echo "Error:" . $q->getMessage();
       }
       $conn = null;
     }
     elseif(isset($_POST['galid'])&& !empty($_POST['galid'])):
     {
       try
       {
         require 'connect.php';
         $galid = test_input($_POST['galid']);
         if($galid == 'All')
          {
            $currslt = $conn->prepare("SELECT SUM(`count`) AS count,DATE_FORMAT(classcount.recdate, '%d %M') AS date FROM `classcount` WHERE classcount.recdate >=  DATE_FORMAT( NOW() ,  '%Y-%m-01' ) GROUP BY classcount.recdate ORDER BY classcount.recdate LIMIT 30");
            $currslt->execute();
            $galgraphrslt=$currslt->fetchAll();
          }
         else
          {
            $currslt = $conn->prepare("SELECT school.studentcount,classcount.`count`,DATE_FORMAT(classcount.recdate, '%d %M') AS date FROM classcount JOIN school ON classcount.galid = school.galid WHERE classcount.galid = :galid AND classcount.recdate >=  DATE_FORMAT( NOW( ) ,  '%Y-%m-01' )ORDER BY classcount.recdate LIMIT 30");
            $currslt->bindParam(':galid', $galid, PDO::PARAM_STR);
            $currslt->execute();
            $galgraphrslt=$currslt->fetchAll();
          }
       }
       catch(PDOException $q)
       {
         echo "Error:" . $q->getMessage();
       }
       $conn = null;
       echo json_encode($galgraphrslt);
    }
   else:
    die();
   endif;
 }
endif;
?>