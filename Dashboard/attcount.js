var infowindow = new google.maps.InfoWindow(),markerx,mapLabel,i,removemarkers = [], removelabels = [];
var bounds = new google.maps.LatLngBounds();
    //var myVar = setInterval(function(){ attcount() }, 2000);
    /*function attcount()
    {
      var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          if(xmlhttp.responseText > $("#schooltdcnt").text())
           {  

              var incrmnt = +xmlhttp.responseText - $("#schooltdcnt").text();
              $("#schooltdcnt").text(+$("#schooltdcnt").text() + incrmnt);
              $("#schoolweekcnt").text(+$("#schoolweekcnt").text() + incrmnt);
              $("#schoolmoncnt").text(+$("#schoolmoncnt").text() + incrmnt);
           }
        }
     };
     xmlhttp.open('POST','attresponse.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('schoolid=schoolid');
    };*/
    

  function init() {
    var map1;
    var mapOptions = {
        streetViewControl: false,
        mapTypeId: 'roadmap'
    };

    // Display a map on the page
    map1 = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map1.setTilt(45);
var icongreen = 'https://maps.google.com/mapfiles/ms/icons/green-dot.png';

var iconred = 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';

var iconorange = 'https://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
var iconblue = 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png';

function refreshloc()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          var response = JSON.parse(xmlhttp.responseText);
          //console.log(xmlhttp.responseText);
          loadmap(response);
        }
     };
     xmlhttp.open('POST','location.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('getschool=getschool');

function loadmap(locations)
{
  for (i = 0; i < locations.length; i++) {

       if(locations.length>0)
       {

          /*markerx = new google.maps.Marker({
           position: new google.maps.LatLng(locations[i].latitude,locations[i].longitude),
           map: map1,
           icon: iconblue
           });*/

        if(locations[i].allcount >= 60)
        {
           markerx = new google.maps.Marker({
           position: new google.maps.LatLng(locations[i].latitude,locations[i].longitude),
           map: map1,
           icon: icongreen
           });
        }
        else if(locations[i].allcount < 40)
        {

           markerx = new google.maps.Marker({
           position: new google.maps.LatLng(locations[i].latitude,locations[i].longitude),
           map: map1,
           animation:google.maps.Animation.BOUNCE,
           icon:iconred
           });
        }
        else
        {
          markerx = new google.maps.Marker({
           position: new google.maps.LatLng(locations[i].latitude,locations[i].longitude),
           map: map1,
           icon:iconorange
           });
        }
        mapLabel = new MapLabel({
            text: locations[i].school,
            position: new google.maps.LatLng(locations[i].latitude,locations[i].longitude),
            map: map1
       });

      google.maps.event.addListener(markerx, 'mouseover', (function (markerx, i) {

        return function () {
             if(locations[i].allcount >= 60)
              {
                infowindow.setContent('Today Missfalls : 2 times<br>Alarms Rang : 5');
              }
             else if(locations[i].allcount >= 40 && locations[i].allcount <= 60)
              {
                infowindow.setContent('Today Missfalls : 2 times<br>Alarms Rang : 5<br><a href="#" style="color:red;text-decoration:none;"><i class="fa fa-circle" style="color:red;"></i> Generate Alert</a><div style="color:red;"><strong>Alert Count = 1</strong></div>');
              }
             else
              {
                infowindow.setContent('Today Missfalls : 2 times<br>Alarms Rang : 5<br><a href="#" style="color:red;text-decoration:none;"><i class="fa fa-circle" style="color:red;"></i> Generate Alert</a><div style="color:red;"><strong>Alert Count = 3</strong></div>');
              }



            infowindow.open(map1, markerx);
        }
    })(markerx, i));

    }
  }
}

}

function refreshloc1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          var response1 = JSON.parse(xmlhttp.responseText);
          loadmap1(response1);
        }
     };
     xmlhttp.open('POST','location.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
      xmlhttp.send('getschool=getschool');

function loadmap1(locations1)
{
  for (i = 0; i < locations1.length; i++) {
   if(locations1.length>0)
     {
        bounds.extend(new google.maps.LatLng(locations1[i].latitude,locations1[i].longitude));
        map1.fitBounds(bounds);

     }
  }
  bounds = new google.maps.LatLngBounds();
}
}
 refreshloc();
 refreshloc1();

}

google.maps.event.addDomListener(window, 'load', init);
