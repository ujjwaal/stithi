<?php

if(!isset($_SERVER['HTTP_X_PJAX'])){

    $content = basename($_SERVER['SCRIPT_NAME']);

    $_SERVER['HTTP_X_PJAX'] = true;
    include 'stilearn.base.template.php';
    die();
}

?>
<?php

  try
  {
    require 'connect.php';
    $user='7828476732';

    if($user == '7828476732')
     {
       $schlrslt = $conn->prepare("SELECT school.galid,school.avgattn,templocation.school FROM `school` JOIN `templocation` ON school.schoolid = templocation.schoolid ORDER BY school.avgattn DESC");   //CLASS WISE COUNT OF LOGGED IN SCHOOL
       $schlrslt->execute();
       $totalschlrslt=$schlrslt->fetchAll();
     }
    else
     {
       $schlrslt = $conn->prepare("SELECT school.galid,school.avgattn,templocation.school FROM `school` JOIN `templocation` ON school.schoolid = templocation.schoolid WHERE school.schoolid = :username");   //CLASS WISE COUNT OF LOGGED IN SCHOOL
       $schlrslt->bindParam(':username', $user, PDO::PARAM_STR);
       $schlrslt->execute();
       $totalschlrslt=$schlrslt->fetchAll();
     }
  }
  catch(PDOException $q)
  {
     echo "Error:" . $q->getMessage();
  }
  $conn = null;

?>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

   <h1 style="text-align:center" id="schoolnm">Patient Adherence Graph</h1>


                            <div id="chart_div"></div><br>
                            <div style="height:300px;overflow: auto;">
                               <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Patient Name</th>
                                            <th>Performance</th>
                                            <th>Average Monthly Take</th>
                                            <th style="width:18%;">See Graph</th>
                                        </tr>
                                    </thead>
                                    <tbody>
    <?php
                                       $i=1;
                                       foreach($totalschlrslt as $val)
                                        {
       $avgatt = $val['avgattn'];
       $val['school'] = str_replace(' ', '', $val['school']);


       if($avgatt < 40)
        {
          echo '<tr class="danger">
                                            <td>'.$i++.'</td>
                                            <td>'.$val['school'].'</td>
                                            <td>-</td>
                                            <td><strong>'.$avgatt.'%</strong></td>
                                            <td><button type="button" class="classdet btn btn-info '.$val['school'].' '.$val['galid'].'">See Graph</button>
                                                <button type="button" style="margin-left:2%" class="showstuddet btn btn-info '.$val['galid'].'">View Details</button>
                                            </td>
                                        </tr>';
        }
       else
        {
          echo '<tr>
                                            <td>'.$i++.'</td>
                                            <td>'.$val['school'].'</td>
                                            <td>-</td>
                                            <td><strong>'.$avgatt.'%</strong></td>
                                            <td><button type="button" class="classdet btn btn-info '.$val['school'].' '.$val['galid'].'">See Graph</button>
                                                <button type="button" style="margin-left:2%" class="showstuddet btn btn-info '.$val['galid'].'">View Details</button>
                                            </td>
                                        </tr>';
        }


                                        }
                                      ?>
                                    </tbody>
                                </table><!-- /table -->
                            </div><!-- /table responsive -->
                            </div>

 <style>
  @media screen and (max-width: 0px) and (min-width: 768px) {
    .chart {
  width: 100%; 
  min-height: 500px;

}
@media screen and (max-width: 769px) and (min-width: 1200px) {
 .chart {
  width: 1200px;
  min-height: 500px;

}

}

 </style>

 <script>

 <?php
  if($user == '7828476732')
   {
     echo "var classid = 'All';
     var schoolname = 'Patient Adherence Graph';
     $('#schoolnm').html(schoolname);";
   }
  else
   {
     echo "var classid = '".$totalschlrslt[0]['galid']."';var schoolname = '".$totalschlrslt[0]['school']."';$('#schoolnm').html('Patient Adherence Graph ('+schoolname+')');";
   }
  ?>


  var attendance,i;

  google.charts.load('current', {packages: ['corechart', 'line']});
  google.charts.setOnLoadCallback(drawBackgroundColor);

function drawBackgroundColor() {

    refreshgraph();

    function refreshgraph()
    {

      var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          attendance = JSON.parse(xmlhttp.responseText);

          drawChart();
        }
     };
     xmlhttp.open('POST','attresponse.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('galid='+classid);
    };

      var data = new google.visualization.DataTable();

      data.addColumn('string', 'X');
      data.addColumn('number', 'Maximum');
      data.addColumn('number', 'Attendance');

      var options = {
        animation:{
         duration: 1000,
         easing: 'in',
        },
        hAxis: {
          title: 'Days'
        },
        vAxis: {
          title: 'Attendance',
        },
        colors: ['#00ff00', '#0088cc']
       // backgroundColor: 'white'
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      var current = 0;

    function drawChart() {
      // Disabling the button while the chart is drawing.
      
      $(".classdet").prop("disabled", true);

       for (i = 1; i <= attendance.length; i++) {

                var attcount = +attendance[i-1].count;
                //console.log(''+attendance[i-1].date);
                if(classid == 'All')
                 {
                   data.addRow([''+attendance[i-1].date,670,attcount]);
                 }
                else
                 {
                   data.addRow([''+attendance[i-1].date,+attendance[i-1].studentcount,attcount]);
                 }
            }

      chart.draw(data, options);
      $(".classdet").prop("disabled", false);
    }
    $(".classdet").click(function()
    {
      schoolname = $(this).attr('class').split(' ')[3] ;
      classid = $(this).attr('class').split(' ')[4] ;
      //console.log(schoolname+' '+classid);
      $("#schoolnm").html("Patient Adherence Graph ("+schoolname+")");
      data.removeRows(0, data.getNumberOfRows());
      refreshgraph();

    });
    }
    
    $(".showstuddet").click(function()
    {
      studdetid = $(this).attr('class').split(' ')[3] ;
      //console.log(studdetid);
      window.open("studentdata.php?classid="+studdetid);
    });
    
    $(window).resize(function(){

  drawBackgroundColor();
});

 </script>