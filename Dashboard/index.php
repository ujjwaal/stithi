﻿<?php

if(!isset($_SERVER['HTTP_X_PJAX'])){

    $content = basename($_SERVER['SCRIPT_NAME']);

    $_SERVER['HTTP_X_PJAX'] = true;
    include 'stilearn.base.template.php';
    die();
}
?>


                    <!-- DASHBOARD
                    ================================================== -->
                    <!-- Dashboard  -->
                    <div id="error-placement"></div>
                    <div class="row margin-top" style="margin-top:-1%">
                        <div class="col-md-12 margin-bottom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#system-stats" data-toggle="tab">Monthly</a></li>
                                <li style="float:right;width:150px;">
                                  <form role="form">
                                    <div class="form-group">
                                      <span class="input-group input-group-in date" data-input="datepicker" data-format="dd/mm/yyyy">
                                        <input id= "pickyDate" class="form-control" type="text" placeholder="Select Date" style="text-align:center;">
                                        <span class="input-group-addon text-silver"><i class="fa fa-calendar"></i></span>
                                      </span><!-- /input-group-in -->
                                     </div><!--/form-group-->
                                  </form>
                                </li>
                            </ul>
                        </div>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="system-stats" class="tab-pane fade active in">
                                <div class="col-md-6">
                                    <div id="overall-visitor" class="panel panel-animated panel-success bg-success">
                                        <div class="panel-body">
                                            <div class="panel-actions-fly">
                                                <a href="graphs.php" title="Go to system stats page" class="btn-panel" style="font-size:15px;"> <span style="font:20px bold georgia;color:white;">See Graph</span>
                                                    <i class="glyphicon glyphicon-stats" style="color:white;"></i>
                                                </a><!--/btn-panel-->
                                                <button data-refresh="#overall-visitor" data-error-place="#error-placement" title="refresh" class="btn-panel">
                                                    <i class="glyphicon glyphicon-refresh"></i>
                                                </button><!--/btn-panel-->
                                            </div><!--/panel-action-fly-->

                                            <p class="lead">Compliant Patients</p><!--/lead as title-->

                                            <ul class="list-percentages row">
                                                <li class="col-xs-4">
                                                  <p class="text-ellipsis">Today</p>
                                                  <strong></span><span id="schooltdcnt" class="text-lg">97%</span></strong>
                                                </li>
                                                <li class="col-xs-4">
                                                    <p class="text-ellipsis">This Week</p>
                                                    <strong></span><span  id="schoolweekcnt" class="text-lg">56%</span></strong>
                                                </li>
                                                <li class="col-xs-4">
                                                    <p class="text-ellipsis">This Month</p>
                                                    <strong></span><span  id="schoolmoncnt" class="text-lg">49%</span></strong>
                                                </li>
                                            </ul><!--/list-percentages-->
                                            <p class="helper-block">
                                                <div class="progress progress-xs progress-flat progress-inverse-inverse">
                                                    <div class="progress-bar progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$todaypercent.'%"' ?> >
                                                        <span class="sr-only"><?php echo $todaypercent ?>% Complete (inverse)</span>
                                                    </div>
                                                </div>
                                                <p class="text-ellipsis"><i class="fa fa-caret-up"></i>  &nbsp; 20<sup>%</sup> <small> Increment, Looks Good! from Last Month</small></p>
                                            </p><!--/help-block-->
                                        </div><!--/panel-body-->
                                    </div><!--/panel overal-visitor-->
                                </div><!--/cols-->

                                <div class="col-md-6">
                                    <div id="overall-users" class="panel panel-animated panel-danger bg-danger">
                                        <div class="panel-body">
                                            <div class="panel-actions-fly">
                                                <a href="#" data-toggle="modal" data-target="#modalLarge" title="Go to system stats page" class="btn-panel" style="font-size:15px;"> <span style="font:20px bold georgia;color:white;">See Graph</span>
                                                    <i class="glyphicon glyphicon-stats" style="color:white;"></i>
                                                </a><!--/btn-panel-->
                                                <button data-refresh="#overall-users" data-error-place="#error-placement" title="refresh" class="btn-panel">
                                                    <i class="glyphicon glyphicon-refresh"></i>
                                                </button><!--/btn-panel-->
                                            </div><!--/panel-action-fly-->

                                            <p class="lead">Non-Compliant</p><!--/lead as title-->

                                            <ul class="list-percentages row">
                                                <li class="col-xs-4">
                                                  <p class="text-ellipsis">Today</p>
                                                  <strong><span  id="schooltdcnt" class="text-lg">50%</span></strong>
                                                </li>
                                                <li class="col-xs-4">
                                                    <p class="text-ellipsis">This Week</p>
                                                    <strong><span  id="schoolweekcnt" class="text-lg">45%</span></strong>
                                                </li>
                                                <li class="col-xs-4">
                                                    <p class="text-ellipsis">This Month</p>
                                                    <strong></span><span  id="schoolmoncnt" class="text-lg">42%</span></strong>
                                                </li>
                                            </ul><!--/list-percentages-->
                                            <p class="helper-block">
                                                <div class="progress progress-xs progress-flat progress-inverse-inverse">
                                                    <div class="progress-bar progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                                        <span class="sr-only">38% Complete (inverse)</span>
                                                    </div>
                                                </div>
                                                <p class="text-ellipsis"><i class="fa fa-caret-down"></i> 15 &nbsp;&nbsp; 10<sup>%</sup> <small>Decrement, Looks not Good! from Last Month</small></p>
                                            </p><!--/help-block-->
                                        </div><!--/panel-body-->
                                    </div><!--/panel overal-users-->
                                </div><!--/cols-->


                            </div><!--/#system-stats-->
                        </div><!--/tab-content-->
                    </div><!--/row-->





                    <div class="row">
                        <div class="col-sm-12">
                            <!-- VMAP -->
                            <div class="panel panel-animated panel-default">
                                <div class="panel-heading bg-white">
                                    <div class="panel-actions">
                                        <div class="btn-group">
                                            <button class="btn-panel dropdown-toggle" data-toggle="dropdown">
                                                Change Maps
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul id="changeMapRegion" class="dropdown-menu pull-right">
                                                <li class="active"><a href="#">Overall</a></li>
                                                <li><a href="#" style="background-color:#669999;color:white">Usage Wise</a></li>
                                                <li><a href="#" >A Grade States</a></li>
                                                <li><a href="#" >B Grade States</a></li>
                                                <li><a href="#" >C Grade States</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" style="background-color:#669999;color:white">Cleanliness Wise</a></li>
                                                <li><a href="#" >A Grade States</a></li>
                                                <li><a href="#" >B Grade States</a></li>
                                                <li><a href="#" >C Grade States</a></li>
                                            </ul>
                                        </div><!--/btn-group-->
                                    </div><!-- /panel-actions -->
                                    <h4 class="panel-title" data-ariginal-title="World Map" id="mapRegion">Implementation</h4>
                                </div><!--/panel-heading-->

                                <div class="panel-body">
                                    <div id="map_canvas" style="height:450px"></div>
                                </div><!--/panel-body-->


                            </div><!--/panel-->
                        </div><!--/cols-->


                    </div><!--/row-->



                           <!-- Modal attendance count weekly -->
                            <div class="modal fade" data-sound = "off" id="modalLarge" tabindex="-1" role="dialog" aria-labelledby="modalLargeLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="modalLargeLabel">Weekly Attendance Graph</h4>
                                        </div>
                                        <div class="modal-body">
                                           <img src="images/attendancegraph.png" alt="Smiley face" width="100%">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->




                           <div class="modal fade" id="modalresource" data-sound = "off" tabindex="-1" role="dialog" aria-labelledby="modalLargeLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="modalLargeLabel">Schools Available for resource Sharing</h4>
                                        </div>

                                        <div class="modal-body">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>School Name</th>
                                            <th>Used Rice</th>
                                            <th>Available Rice</th>
                                            <th>Request Rice</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Pathadi</td>
                                            <td>100KG</td>
                                            <td>60KG</td>
                                            <td><button type="button" class="btn btn-inverse">Request</button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Lekhanagar</td>
                                            <td>90KG</td>
                                            <td>50KG</td>
                                            <td><button type="button" class="btn btn-inverse">Request</button></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Mukthidham</td>
                                            <td>95KG</td>
                                            <td>45KG</td>
                                            <td><button type="button" class="btn btn-inverse">Request</button></td>
                                        </tr>
                                       <tr>
                                            <td>4</td>
                                            <td>Kamathwadi</td>
                                            <td>110KG</td>
                                            <td>30KG</td>
                                            <td><button type="button" class="btn btn-inverse">Request</button></td>
                                        </tr>
                                    </tbody>
                                </table><!-- /table -->
                            </div><!-- /table responsive -->

                                        </div><!--Modal Body-->

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->



 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-THie7oYQU3wlR9V2ZsmuGJaR9X4xnbc"></script>
 <script src="attcount.js"></script>
 <script src="maplabel.js"></script>
 
 <script>
  $(document).ready(function()
  {
    $('#pickyDate').one("change", function () 
     {
      // tddate = $('#pickyDate').val();
       attcount();

     });

     function attcount()
      {
        console.log($('#pickyDate').val());
      }
  });
 </script>