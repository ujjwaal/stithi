<?php
 require 'logininfo.php';
  if(loggedin()==false):
 {die(header('Location:index.php'));}
  endif;

  function test_input($data)
  {
   $data = stripslashes($data);
   $data = htmlentities($data);
   $data = htmlspecialchars($data);
   $data = ucwords(strtolower($data));
   return $data;
  };
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Stithi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Stithi - Monitoring Health">

        <meta http-equiv="x-pjax-version" content="v173">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!-- fav and touch icons -->
        <link rel="apple-touch-icon" sizes="57x57" href="ico/vidyarohaico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="ico/vidyarohaico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="ico/vidyarohaico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="ico/vidyarohaico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="ico/vidyarohaico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="ico/vidyarohaico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="ico/vidyarohaico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="ico/vidyarohaico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="ico/vidyarohaico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="ico/vidyarohaico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="ico/vidyarohaico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="ico/vidyarohaico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="ico/vidyarohaico/favicon-16x16.png">
        <link rel="manifest" href="ico/vidyarohaico/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="ico/vidyarohaico/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- build:css styles/vendor.css -->
        <!-- bower:css -->
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="bower_components/animate.css/animate.css">
        <link rel="stylesheet" href="bower_components/hover/css/hover.css">
        <!-- endbower -->
        <!-- endbuild -->
        
        <!-- build:css(.tmp) styles/main.css -->
        <link id="style-components" href="styles/loaders.css" rel="stylesheet">
        <link id="style-components" href="styles/bootstrap-theme.css" rel="stylesheet">
        <link id="style-components" href="styles/dependencies.css" rel="stylesheet">
        <link id="style-base" href="styles/stilearn.css" rel="stylesheet">
        <link id="style-responsive" href="styles/stilearn-responsive.css" rel="stylesheet">
        <link id="style-helper" href="styles/helper.css" rel="stylesheet">
        <link id="style-sample" href="styles/pages-style.css" rel="stylesheet">
        <!-- endbuild -->
        
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
        <![endif]-->
        <style>
          @media screen and (min-width: 0px) and (max-width: 767px)
          {
            .vidyarohalogo{
             font:bold 20px georgia;
             }
          }
        @media screen and (min-width: 768px)
          {
            .vidyarohalogo{
              font:bold 30px georgia;
             }
          }
        </style>

       <!-- build:js scripts/vendor-main.js (We put this here so that all the basic jquery and bootstrap script can be loaded first before any other scripts needing this)-->
        <!-- bower:js -->
        <script src="bower_components/jquery/jquery.js"></script>
        <script src="bower_components/jqueryui/ui/jquery-ui.js"></script>
        <script src="bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <!-- endbuild --> 



    </head>

    <body class="animated fadeIn">

        <!-- section header -->
        <header class="header">

          <a href="index.php" class="vidyarohalogo"><img class="brand-logo" style="margin-left:8px;margin-top:8px;width:3%;" src="images/logo.png" alt="Stithi Logo" ><span style="color:#006699;"> STITHI</span></a>


            <!-- header-profile -->
            <div class="header-profile">
                <div class="profile-nav">
                    <span class="profile-username">PHC</span>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu animated flipInX pull-right" role="menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-envelope"></i> Inbox</a></li>
                        <li><a href="#"><i class="fa fa-tasks"></i> Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </div>
                <div class="profile-picture">
                    <img alt="me" src="images/school.jpg">
                </div>
            </div><!-- header-profile -->

            <form role="form" class="form-inline">
                <button type="button" class="btn btn-default btn-expand-search"><i class="fa fa-search"></i></button>
                <div class="toggle-search">
                    <input type="text" class="form-control" placeholder="Search something" />    
                    <button type="button" class="btn btn-default btn-collapse-search"><i class="fa fa-times"></i></button>
                </div>
            </form><!--/form-search-->

            <!-- header menu -->
            <ul class="hidden-xs header-menu pull-right">

                <li>
                    <a href="#" title="Notifications" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <span class="badge badge-success">4</span>
                        <i class="header-menu-icon icon-only fa fa-warning"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-extend animated fadeInDown pull-right" role="menu">
                        <li class="dropdown-header">Notofications</li><!-- /dropdown-header -->
                        <li class="notif-minimal" data-toggle="niceScroll" data-scroll-cursorcolor="#ecf0f1">
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-primary">
                                    <i class="fa fa-heart-o"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">Government</span> Notification From Government</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-warning">
                                    <i class="fa fa-user"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">Funds</span> Notification Regarding Funds</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-success">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">School</span> Notification Regarding School Performance</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-danger">
                                    <i class="fa fa-comment-o"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">MDM</span> Notification For MDM refillment</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-info">
                                    <i class="fa fa-twitter"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">Scholarships</span> Notification For Scholarship Count</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-ico bg-success">
                                    <i class="fa fa-cloud-upload"></i>
                                </div>
                                <p class="notif-text"><span class="text-bold">Login</span> Notification for less Login</p>
                            </a><!-- /notif-item -->
                        </li><!-- /dropdown-alert -->
                        <li class="dropdown-footer bg-cloud">
                            <a class="view-all" tabindex="-1" href="#">
                                <i class="fa fa-angle-right pull-right"></i> See all notifications
                            </a>
                        </li><!-- /dropdown-footer -->
                    </ul><!-- /dropdown-extend -->
                </li><!-- /header-menu-item -->
                <li>
                    <a href="#" title="Inboxs" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <span class="badge badge-warning animated animated-repeat flash">3</span>
                        <i class="header-menu-icon icon-only fa fa-envelope-o"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-extend animated fadeInDown pull-right" role="menu">
                        <li class="dropdown-header">You have 3 new messages</li><!-- /dropdown-header -->
                        <li class="notif-media" data-toggle="niceScroll" data-scroll-cursorcolor="#ecf0f1">
                            <a class="notif-item" href="#">
                                <div class="notif-img pull-left">
                                    <img src="images/govt.jpg" alt="" class="img-circle" />
                                </div>
                                <h3 class="notif-heading">Government <small>58 min</small></h3>
                                <p class="notif-text">Message 1</p>
                            </a><!-- /notif-item -->
                            <a class="notif-item" href="#">
                                <div class="notif-img pull-left">
                                    <img src="images/teacher.jpg" alt="" class="img-circle" />
                                </div>
                                <h3 class="notif-heading">Teacher (XYZ)(2) <small>Wed</small></h3>
                                <p class="notif-text">Some Message</p>
                            </a><!-- /notif-item -->

                        </li><!-- /dropdown-alert -->
                        <li class="dropdown-footer bg-cloud">
                            <a class="view-all" tabindex="-1" href="#">
                                <i class="fa fa-angle-right pull-right"></i> See all messages
                            </a>
                        </li><!-- /dropdown-footer -->
                    </ul><!-- /dropdown-extend -->
                </li><!-- /header-menu-item -->

            </ul><!--/header-menu pull-right-->
        </header><!--/header-->

        
        <!-- content section -->
        <section class="section">
            <!-- DONT FORGET REPLACE IT FOR PRODUCTION! -->
            <aside class="side-left">
                <ul class="sidebar">
                    <li>
                        <a href="index.php">
                            <i class="sidebar-icon fa fa-home"></i>
                            <span class="sidebar-text">Dashboard</span>
                        </a>
                    </li><!--/sidebar-item-->


                    <li>
                        <a href="#" >
                            <i class="sidebar-icon fa fa-bar-chart-o"></i>
                            <span class="sidebar-text">Analysis</span>
                        </a>
                        <ul class="sidebar-child animated flipInY">
                            <li>
                                <a href="dropout.php" >
                                    <span class="sidebar-text">Non-Adherence Patterns</span>
                                </a>
                            </li><!--/child-item-->
                            
                            <li class="divider"></li>

                            <li>
                                <a href="chronic.php">
                                    <span class="sidebar-text">Chronic Missfalls</span>
                                </a>
                            </li><!--/child-item-->
                        </ul><!--/sidebar-child-->
                    </li><!--/sidebar-item-->
                    <li>
                        <a href="#">
                            <i class="sidebar-icon fa fa-table"></i>
                            <span class="sidebar-text">Forms</span>
                        </a>
                        <ul class="sidebar-child animated flipInY">
                            <li>
                                <a href="scholarshipform.php">
                                    <span class="sidebar-text">Survey Form</span>
                                </a>
                            </li><!--/child-item-->
                            <li class="divider"></li>
                            <li>
                                <a href="mdm.php">
                                    <span class="sidebar-text">Status form</span>
                                </a>
                            </li><!--/child-item-->

                        </ul><!--/sidebar-child-->
                    </li><!--/sidebar-item-->

                    <li>
                        <a href="#">
                            <i class="sidebar-icon fa fa-building-o"></i>
                            <span class="sidebar-text">States</span>
                        </a>
                    </li><!--/sidebar-item-->
                    
                    <li>
                        <a href="calendar.php">
                            <i class="sidebar-icon fa fa-calendar-o"></i>
                            <span class="sidebar-text">Calendar</span>
                        </a>
                    </li><!--/sidebar-item-->
                    
                    <li>
                        <a href="#">
                            <i class="sidebar-icon fa fa-file-text"></i>
                            <span class="sidebar-text">Reports</span>
                        </a>
                        <ul class="sidebar-child animated flipInY">
                            <li>
                                <a href="academicreport.php">
                                    <span class="sidebar-text">Academic Report</span>
                                </a>
                            </li><!--/child-item-->
                            <li class="divider"></li>
                            <li>
                                <a href="weeklyreport.php">
                                    <span class="sidebar-text">Weekly Performance</span>
                                </a>
                            </li><!--/child-item-->
                        </ul><!--/sidebar-child-->

                    </li><!--/sidebar-item-->



                </ul><!--/sidebar-->
            </aside><!--/side-left-->

            <div class="content">
                <div class="content-header">
                    <div class="content-header-extra">
                        <a href="#" data-toggle="modal" data-target="#modalclass" title ="Classes Needing extra Attention" style="margin-left:10px;margin-right:10px;color:red;"><i class="fa fa-eye" style="font-size:24px;color:red"></i></a><!--/item-ch-extra-->
                        <a href="#" rel="popover-bottom" data-container="body" data-context="primary" data-trigger="hover" title="Scholarship Eligible Students" data-content="Will Be Updated Soon !" style="margin-right:10px;"><i class="fa fa-group" style="font-size:24px;color:#008080;"></i></a><!--/item-ch-extra-->
                        <a href="#" style="margin-right:10px;" rel="popover-bottom" data-container="body" data-context="primary" data-trigger="hover" title="PSM Status" data-content="Will Be Updated Soon!"><i class="fa fa-tachometer" style="font-size:24px;color:#008080;"></i></a><!--/item-ch-extra-->
                    </div><!--/content-header-extra -->

                    <h2 class="content-title"><i class="fa fa-home"></i> Welcome to STITHI</h2>
                </div><!--/content-header -->

                <div class="content-body">
                    <!-- APP CONTENT
                    ================================================== -->
                    <?php 
                        if(isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == 'true'){
                            include($content);
                        }
                        else{
                            include('index.php');
                        }
                    ?>
                </div><!--/content-body -->
            </div><!--/content -->

        </section><!--/content section -->
        



        <!-- section footer -->
        <a rel="to-top" href="#top"><i class="fa fa-arrow-up"></i></a>
        <footer>
            <p>&copy; 2018 Stithi</p>
        </footer>



        <!-- javascript
        ================================================== -->
        <!-- List of dependencies file, please check on readme.txt! (Purchase only) -->

        <!-- build:js scripts/vendor-usefull.js -->
        <script src="bower_components/pace/pace.min.js"></script>
        <script src="bower_components/jquery-pjax/jquery.pjax.js"></script>
        <script src="bower_components/masonry/masonry.pkgd.min.js"></script>
        <script src="bower_components/screenfull/dist/screenfull.min.js"></script>
        <script src="bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="bower_components/countUp.js/countUp.min.js"></script>
        <script src="bower_components/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="bower_components/wow/dist/wow.min.js"></script>
        <!-- endbuild -->

        <!-- build:js scripts/vendor-form.js -->
        <script src="bower_components/jquery.validation/jquery.validate.js"></script>
        <script src="bower_components/jquery.validation/additional-methods.js"></script>
        <script src="bower_components/autogrow-textarea/jquery.autogrowtextarea.min.js"></script>
        <script src="bower_components/typeahead.js/dist/typeahead.min.js"></script>
        <script src="bower_components/jQuery-Mask-Plugin/jquery.mask.min.js"></script>
        <script src="bower_components/jquery.tagsinput/jquery.tagsinput.min.js"></script>
        <script src="bower_components/multiselect/js/jquery.multi-select.js"></script>
        <script src="bower_components/select2/select2.js"></script>
        <script src="bower_components/jquery-selectboxit/src/javascripts/jquery.selectBoxIt.js"></script>
        <script src="bower_components/momentjs/moment.js"></script>
        <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script src="bower_components/jquery-minicolors/jquery.minicolors.min.js"></script>
        <script src="bower_components/dropzone/downloads/dropzone.min.js"></script>
        <script src="bower_components/jquery-steps/build/jquery.steps.min.js"></script>
        <script src="bower_components/fullcalendar/fullcalendar.js"></script>
        <!-- endbuild -->
        
        <!-- build:js scripts/vendor-editor.js -->
        <script src="bower_components/wysihtml5/dist/wysihtml5-0.3.0.js"></script>
        <script src="bower_components/bootstrap-wysihtml5/dist/bootstrap-wysihtml5-0.0.2.js"></script>
        <script src="bower_components/bootstrap-markdown/js/markdown.js"></script>
        <script src="bower_components/bootstrap-markdown/js/to-markdown.js"></script>
        <script src="bower_components/bootstrap-markdown/js/bootstrap-markdown.js"></script>
        <!-- endbuild -->
        
        
        <!-- build:js scripts/excanvas.js -->
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="bower_components/flot/excanvas.min.js"></script><![endif]-->
        <!-- endbuild -->

        <!-- build:js scripts/vendor-graph.js -->
        <script src="bower_components/raphael/raphael-min.js"></script>
        <script src="bower_components/morris.js/morris.min.js"></script>
        <script src="bower_components/flot/jquery.flot.js"></script>
        <script src="bower_components/flot/jquery.flot.resize.js"></script>
        <script src="bower_components/flot/jquery.flot.categories.js"></script>
        <script src="bower_components/flot/jquery.flot.time.js"></script>
        <script src="bower_components/flot-axislabels/jquery.flot.axislabels.js"></script>
        <script src="bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script>
        <script src="bower_components/sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- endbuild -->
    
        <!-- build:js scripts/vendor-table.js -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.js"></script>
        <script src="bower_components/datatables-tools/js/dataTables.tableTools.js"></script>
        <script src="bower_components/datatables-bootstrap3/bs3/assets/js/datatables.js"></script>
        <script src="bower_components/jquery.tablesorter/js/jquery.tablesorter.js"></script>
        <script src="bower_components/jquery.tablesorter/js/jquery.tablesorter.widgets.js"></script>
        <script src="bower_components/jquery.tablesorter/addons/pager/jquery.tablesorter.pager.js"></script>
        <!-- endbuild -->


        <!-- build:js scripts/vendor-util.js -->
        <script src="bower_components/holderjs/holder.js"></script>
        <!-- endbower -->
        <!-- endbuild -->


        <!-- required stilearn template js -->
        <!-- build:js scripts/main.js -->
        <script src="scripts/bootstrap-jasny/js/fileinput.js"></script>
        <script src="scripts/js-prototype.js"></script>
        <script src="scripts/slip.js"></script>
        <script src="scripts/hogan-2.0.0.js"></script>
        <script src="scripts/theme-setup.js"></script>
        <script src="scripts/chat-setup.js"></script>
        <script src="scripts/panel-setup.js"></script>
        <!-- endbuild -->

        <!-- This scripts will be reload after pjax or if popstate event is active (use with class .re-execute) -->
        <!-- build:js scripts/initializer.js -->

        <script class="re-execute" src="scripts/bootstrap-setup.js"></script>
        <script class="re-execute" src="scripts/jqueryui-setup.js"></script>
        <script class="re-execute" src="scripts/dependencies-setup.js"></script>
        <!-- endbuild -->
        
        <div class="modal fade" id="modalclass" data-sound = "off" tabindex="-1" role="dialog" aria-labelledby="modalLargeLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="modalLargeLabel">Classes Needing Extra Attention (Month Wise Performance)</h4>
                                        </div>

                                        <div class="modal-body">
                            
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Class</th>
                                            <th>Academic Performance</th>
                                            <th>Average Attendance</th>
                                            <th>See Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Class 4</td>
                                            <td>45%</td>
                                            <td>50%</td>
                                            <td><button type="button" class="btn btn-danger">See Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Class 7</td>
                                            <td>55%</td>
                                            <td>55%</td>
                                            <td><button type="button" class="btn btn-danger">See Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Class 5</td>
                                            <td>60%</td>
                                            <td>62%</td>
                                            <td><button type="button" class="btn btn-danger">See Details</button></td>
                                        </tr>
                                       <tr>
                                            <td>4</td>
                                            <td>Class 2</td>
                                            <td>67%</td>
                                            <td>70%</td>
                                            <td><button type="button" class="btn btn-danger">See Details</button></td>
                                        </tr>
                                    </tbody>
                                </table><!-- /table -->
                            </div><!-- /table responsive -->

                                        </div><!--Modal Body-->

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

    </body>
</html>
