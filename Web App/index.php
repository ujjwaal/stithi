<!DOCTYPE html>
<html lang="en">
<head>
  <title>STITHI</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" type="text/css" href="bootstrap-datepicker-master/dist/css/bootstrap-datepicker.css" />
  <script src="bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>
  
  <script type="text/javascript">
    $(document).ready(function () {
        $('#pickyDate').datepicker({
            format: "yyyy-mm-dd",
            "todayBtn" : true,
            "todayHighlight" : true,
            "autoclose": true
        });
    });
   </script>

  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
        
    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">STITHI</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#">STITHI</a></li>
      </ul>
    </div>

  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav hidden-xs">
      <h2>STITHI</h2>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="#">STITHI</a></li>
        <li><img class="img-responsive" src="images/nasik.jpg" alt="nasik map" width="300" height="400"></li>
      </ul><br>
    </div>

    <br>

    <div class="col-sm-9">

      <div class="well" style="background-color:#0052cc;">
        <h4 id = "schoolname" style="text-align:center;color:white;font:30px bold georgia">STITHI</h4>
         <br>
        <a href="#" target="_blank" type="button" class="btn btn-default">Analytics</a>
        <form>
         <div class="form-group">
          <input class="form-control" style="float:right;width:20%;margin-top:-4%;" type="text" placeholder="Select date"  id="pickyDate"/>
         </div>
        </form>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="well" style="background-color:#cce0ff;">
            <h4>Total Devices</h4>
            <p style="color:red;font:bold 25px georgia">3</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well" style="background-color:#cce0ff;">
            <h4>Usage Count</h4>
            <p id="stud" style="color:green;font:bold 25px georgia">loading...</p>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-sm-12">
          <div class="well" style="background-color:#1a75ff;">
            <span id = "users" style="font:20px bold georgia;"><span>
          </div>
          <a href="#" style="margin-left:40%;" type="button" class="btn btn-primary">Download Report</a>
        </div>

      </div>

    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready(function()
  {
    var tddate = <?php date_default_timezone_set("Asia/Kolkata"); echo "'".date('Y-m-d')."'"; ?>;

    $('#pickyDate').val(tddate);
    $('#pickyDate').change(function()
     {
       tddate = $('#pickyDate').val();
       attcount();
     });

    var myVar = setInterval(function(){ attcount() }, 1000);
    function attcount()
    {
      $("#stud").text('loading...');

      var xmlhttp;
    if(window.XMLHttpRequest)
     {
       xmlhttp=new XMLHttpRequest();//for mordern browsers
     }
    else
     {
       xmlhttp =new ActiveXObject ('Microsoft.XMLHTTP');//for old browsers
     }
     xmlhttp.onreadystatechange = function()//checking for a state change
     {
       if(xmlhttp.readyState==4 && xmlhttp.status == 200)//weather file is empty or not
        {
          var result = xmlhttp.responseText.split("#");

          $("#stud").text(result[0]);
          $("#users").html(result[1]);
        }
     };
     xmlhttp.open('POST','getcount.php',true);
     xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
     xmlhttp.send('get=get');
    };
  });
</script>

</body>
</html>
