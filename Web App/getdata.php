<?php

if(isset($_GET['string'])) :
{
$items = $_GET['string'];
$items = explode("/",$items);
$deviceid = $items[0];
$alarm = $items[1];
$snooz = $items[2];
$knob = $items[3];

  try
   {
      require 'connect.php';
      $query = $conn->prepare("INSERT INTO `alarms`(`deviceid`, `alarm`, `snooz`, `knob`) VALUES (:device,:alarm,:snooz,:knob)");
      $query->bindParam(':device', $deviceid, PDO::PARAM_STR);
      $query->bindParam(':alarm', $alarm, PDO::PARAM_STR);
      $query->bindParam(':snooz', $snooz, PDO::PARAM_STR);
      $query->bindParam(':knob', $knob, PDO::PARAM_STR);
      $query->execute();
   }
  catch(PDOException $q)
   {
     echo "Error:" . $q->getMessage();
   }
  $conn = null;
}
endif;

?>