/**********************************SLEEP MODE*************************************************/
void sleepNow()
{
  //Sleep mode
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    cli();
    //Disable the Watch dog
    WDTCSR = 0x00;
    //Disable the ADC
    ADCSRA &= ~(1<<7);    //Disable ADC
    ACSR = (1<<ACD);    //Disable the analog comparator
    DIDR0 = 0x7E;     //Disable digital input buffers on all ADC0-ADC5 pins
    sei();          //Enable all interrupt
    sleep_enable();     //Enable sleep
    pinMode(2,INPUT_PULLUP);
   attachInterrupt (digitalPinToInterrupt (2), wakeUpNow, LOW);  // wake up on low level on D2*falling
 
   attachInterrupt (digitalPinToInterrupt (3), wakeUpNow, FALLING);  // wake up on low level on D3
   sleep_mode ();            // here the device is actually put to sleep!!
   detachInterrupt (digitalPinToInterrupt (2));      // stop LOW interrupt on D2
     detachInterrupt (digitalPinToInterrupt (3));
    sleep_bod_disable();  //Disable BOD before after sleep
    sleep_cpu();      //Sleep CPU after BOD Disable
 
}

/**************************************WAKE-UP*********************************************************/
 void wakeUpNow()
{
  sleep_disable ();         // first thing after waking from sleep:
 
}

/**********************************SLEEP WITH SYSTEM *************************************************/
void sleepsystem()
{
   //Serial.println(h);
     //Serial.print(":");
     //Serial.println(m);
  digitalWrite(awake,LOW);//gsm gets off
  delay(100);
  digitalWrite(mosfet,LOW);// rest system off
  delay(100);
  digitalWrite(interruptPin,HIGH);//interrpt0 pin goes high
  digitalWrite(Switch,HIGH);//interrpt1 pin goes high   
  delay(100);
  sleepNow();//system on sleep 
}
/*****************************************SYSTEM WAKEUP*********************************************/
void systemwakeup()
 {
    delay(1000);
    digitalWrite(awake,HIGH);//make GSM ON
    delay(500);
    digitalWrite(mosfet,HIGH);//make sensor, screen, buzzer on active state
    delay(500);
 }
/*************************************************************************************************************/
