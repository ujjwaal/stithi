/********************************ALARM LOOP - FUNCTION WAKES UP AFTER SLEEP************************************************/
void alarm()
{
     systemwakeup();
     /************************************LOOP 1*********************************/
     if(uno==0)//before alarm if knob rotated/button pressed 
      {
       delay(2000);
       sensorValue = digitalRead(sensorPin);// check medicine in slot
       if(sensorValue == 0)
        {
         if( cntalrm<=3)//if medicine is there, four time only buzzer ring
          {
           for(i=0;i<80;i++)// i= 48 tends to 1 min buzzer ring
            {
             sensorValue = digitalRead(sensorPin);
             if(sensorValue == 0)
              {
               alarmtone();
              }
              else//if it is taken then send success message
              {
              //Serial.println("success msg send");
              success_state();
              break;
              }
            }
          }
        }
       sensorValue = digitalRead(sensorPin);
       if(sensorValue == 0)//medicine still there, alert system ON for 6 time
        {
          //Serial.println("fail msg send");
         fail_state();
        }
       else
        { 
          delay(2000);
         success_state();
        }
        sleepsystem();
      }
   
     /************************************LOOP 2*********************************/
     else
       {
        //Serial.println(h);
       //Serial.print(":");
       //Serial.println(m);
       //Serial.println("loop2");
       //knob still not rotate
        if( cntalrm<=3 )
          {
           for(i=0;i<80;i++)
             {
              knobValue = digitalRead(Switch);
              if(knobValue == 0)//knob rotate, stop ringing alarm
                {
                 uno=0;
                 break;
                }
               alarmtone();
             }
          }
         /************if knob rotate****************************************************************/
           if(uno==0)
             { 
               delay(500);
               pausesys();
               //Serial.println("changeValue :");   
               //Serial.print(changeValue);               
               //Serial.println("watch signal sending");
              
               changeValue = digitalRead(sensorPin);
         /***********medicine still there****************/   
                if(changeValue ==0)
                 {
                  uno = 0;
                 //Serial.println("fail signal sending"); 
                 fail_state();
                 }
          /***********medicine taken************************/
                else
                 {
                  if(changeValue == 1)
                   {
                    //Serial.println("success signal sending");
                    success_state();
                   }
               // arrange_time();
               // set_alarm();
                 }
              }
         /***********knob still not rotate**********************************************************/
              else
              {
               //Serial.println("fail signal sending"); 
               fail_state();
              }
         sleepsystem();
      }
 }
