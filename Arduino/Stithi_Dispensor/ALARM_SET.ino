/******************************************SET ALARM*****************************************/
void set_alarm(void)
{
    uint8_t flags[5] = { 0, 0, 0, 1, 1 };
    DS3231_set_a1(s, m, h, 0, flags);
    DS3231_set_creg(DS3231_INTCN | DS3231_A1IE);
}
/*****************************************TRANSLATION OF MINUTE AND HOUR***************************************************/
//translation of minute and hour   
 void arrange_time()
 {
  if (m1 > 59)
  {
   m1 %= 60;
   h1 += 1;
  }
  if(h1>23)
  {h1 %= 24;}
  if (m2 > 59)
  {
   m2 %= 60;
   h2 += 1;
  }
  if(h2>23)
  {h2 %= 24;}
  if (m3 > 59)
  {
   m3 %= 60;
   h3 += 1;
  }
  if(h3>23)
  {h3 %= 24;}
 }
/**********************************SET ALARM THROUGH BUTTON***************************************************/
void set_alarm_button()
 {
  while(iterator == 0)
  {
   state1=digitalRead(set);//button 1 - for alarm set option
    if(state1 == 0)
    {
      almstt+=1;
      if(almstt > 3)
       {
         almstt = 1;
       }
      if(almstt == 1)
       {
         display_text("SET ALARM 1");
         display.display();
       }
      else if(almstt == 2)
       {
         display_text("SET ALARM 2");
         display.display();
       }
      else
       {
         display_text("SET ALARM 3");
         display.display();
       }  
      //delay(500);
    }
  state2=digitalRead(inchr);//button 2 - for hour increment
    if(state2 == 0)
    { 
      display_text("SET HOUR");
      if(almstt == 1)
       {
          h1 += 1;
          display.print(h1);
          display.print(":");
          display.print(m1);
          display.print(":");
          display.print(s1);
          display.display();
       }
      else if(almstt == 2)
       {
          h2 += 1;
          display.print(h2);
          display.print(":");
          display.print(m2);
          display.print(":");
          display.print(s2);
          display.display();
       }
      else
       {
          h3 += 1;
          display.print(h3);
          display.print(":");
          display.print(m3);
          display.print(":");
          display.print(s3);
          display.display();
       } 
      //delay(500);
    }
   state3=digitalRead(incmn);//button 3 - for minute increment
    if(state3 == 0)
    {
      display_text("SET MIN");
      if(almstt == 1)
       {
          m1 += 1;
          display.print(h1);
          display.print(":");
          display.print(m1);
          display.print(":");
          display.print(s1);
          display.display();
       }
      else if(almstt == 2)
       {
          m2 += 1;
          display.print(h2);
          display.print(":");
          display.print(m2);
          display.print(":");
          display.print(s2);
          display.display();
       }
      else
       {
          m3 += 1;
          display.print(h3);
          display.print(":");
          display.print(m3);
          display.print(":");
          display.print(s3);
          display.display();
       } 
   
    }
     state4=digitalRead(setok); //button 4- for OK
    if(state4 == 0)
    {
      m=m1;
      h=h1;
      display_text("ALARM SET");
      display.display();
      display.clearDisplay();
      delay(500);
      display.display();
      set_alarm();
      break;
      //Serial.println("system going to sleep on initialization1");
    }
    arrange_time();
    delay(100);
  }
  //Serial.println("system going to sleep on initialization");
  sleepsystem();
  }
/*******************************************ALARM TONE*************************************************************/ 
void alarmtone()
{
  
                 display_text("MEDICINE");
                display.display();
                digitalWrite(sound, HIGH);
                delay(90);
                digitalWrite(sound, LOW);
                delay(30);
                digitalWrite(sound, HIGH);
                delay(90);
                digitalWrite(sound, LOW);
                display.clearDisplay();
                display.display();
                delay(500); 
 }
