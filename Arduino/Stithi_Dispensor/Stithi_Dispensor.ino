
#include <Adafruit_SSD1306.h>
#define OLED_RESET 4
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2
#include <SoftwareSerial.h>
#define TX 5
#define RX 6

/******************************************INITIALIZATION*****************************************/
SoftwareSerial GSM_8(TX ,RX);
#include <avr/sleep.h>
#include <avr/power.h>
const byte interruptPin = 2;
const byte Switch = 3;
#include <Wire.h>
#include "ds3231.h"
#include <Adafruit_SSD1306.h>
uint8_t iterator =0;
uint8_t h =0;
uint8_t m =0;
uint8_t s =0;
uint8_t deviceid =77;
uint8_t dayStatus=0;//0-Watch,1-success,2-fail
uint8_t h1 =0;
uint8_t m1 =0;
uint8_t s1 =0;

uint8_t h2 =0;
uint8_t m2 =0;
uint8_t s2 =0;

uint8_t h3 =0;
uint8_t m3 =0;
uint8_t s3 =0;

uint8_t almstt =0;
uint8_t alarmnumber = 0;
// time when to wake up

// how often to refresh the info on stdout (ms)
unsigned long prev = 5000, interval = 5000;
#define LOGO16_GLCD_HEIGHT 16
#define LOGO16_GLCD_WIDTH  16
Adafruit_SSD1306 display(OLED_RESET);
#define set 8
#define inchr 9
#define incmn 10
#define setok 11
#define sound 13
#define sensorPin 7
uint8_t sensorValue = 1 ;
uint8_t changeValue = 0 ;
uint8_t knobValue = 1 ;
const int awake=4;
const int mosfet=12;
uint8_t i;
uint8_t j;

uint8_t uno = 1 ;
uint8_t state1;
uint8_t state2;
uint8_t state3;
uint8_t state4;
uint8_t rlhr;
uint8_t rlm;
uint8_t rls;
uint8_t u=0;
uint8_t count = 0;
uint8_t cntalrm =0;

int battery = 200;
/*******************************SETUP- RUN ONCE AFTER RESET****************************************************/
void setup()
{
 
 ////Serial.begin(9600);
 Wire.begin();// I2C communication via RTC and OLED
 GSM_8.begin(9600);//communication with GSM @ 9600 baud rate
 pinMode(interruptPin,INPUT_PULLUP);//assign interrupt 0
 pinMode(Switch,INPUT_PULLUP);//assign interrupt 1
 pinMode(sound,OUTPUT);//assign buzzer
 pinMode(awake,OUTPUT);//assign GSM enable pin
 digitalWrite(awake,HIGH);//make GSM ON
 pinMode(mosfet,OUTPUT);//assign other devices accept RTC & reed Switch via mosfet
 digitalWrite(Switch,HIGH);//assign reed Switch
 digitalWrite(mosfet,HIGH);//make other system ON
 DS3231_init(DS3231_INTCN);//initialize RTC
 DS3231_clear_a1f();//clear all its flag before set
 display.begin();//display in OLED
 display_text(" STITHI ");
 display.display();
 display.startscrollright(0x00, 0x0F);//scroll functionality
 delay(4500);
 display.stopscroll();
 delay(500);
}


  /************************************MAIN LOOP- RUN FOREVER*****************************************/
void loop()
{
  
  unsigned long now = millis();
  struct ts t;
  if (iterator == 0)
   {
    set_alarm_button();
    delay(500);
    iterator++;
    }
      if ((now - prev > interval))
  {
   DS3231_get(&t);
     if (DS3231_triggered_a1()) //when set time = RTC time, trigger happen
     {
      DS3231_clear_a1f();// clear prev alarm SET flag
      delay(50);
      alarm();//go to alarm loop
     }
      prev = now;
     }
   arrange_time();
   rlhr = t.hour;
   rlm = t.min;
   rls =t.sec;
   
//trigger of knob value   
   knobValue = digitalRead(Switch);
    if(knobValue == 0)
 {
   uno = 0;
   delay(200);
  }

// if soeone takes pill during sleep mode
    knobValue = digitalRead(Switch);
   if(knobValue == 0)
   {
    systemwakeup();
    uno = 0;
    changeValue = digitalRead(sensorPin);// IR sensor value
    delay(500);
    pausesys();
    dayStatus=0;
    delay(100);
    //Serial.println("watch signal tx");
    sendsms();//send watch flag
    //Serial.println("watch signal sent");
    //Serial.print(changeValue);
    digitalWrite(13,HIGH);
    delay(100);
    digitalWrite(13,LOW);
    changeValue = digitalRead(sensorPin);// read sensor value
    delay(1000);
     if( changeValue ==1 )//if medicine taken and no success message came
     {
     //Serial.print("success sig sending");
     success_state();
     //Serial.print("system went to sleep");
     }
     //Serial.println(h);
     //Serial.print(":");
     //Serial.println(m);
     
     sleepsystem();
   }
}
